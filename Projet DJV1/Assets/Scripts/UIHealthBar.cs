using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHealthBar : MonoBehaviour
{
    public float fillStatus;
    // Start is called before the first frame update
    void Start()
    {
        fillStatus = 1;
    }

    // Update is called once per frame
    void Update()
    {
        RectTransform fillTransform = transform.GetChild(0).transform.GetChild(0).GetComponent<RectTransform>();
        fillTransform.anchorMax =new Vector2(fillStatus, fillTransform.anchorMax.y);
    }

    public void SetFillStatus(float newStatus)
    {
        fillStatus = newStatus;
    }


}
