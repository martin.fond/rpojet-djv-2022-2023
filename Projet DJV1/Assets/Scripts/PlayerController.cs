using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Movement

    public float baseMoveSpeed;
    public float runningSpeed;
    public bool  landed;
    
    private float moveSpeed;
    private bool isRunning;
    public float jumpSpeed;
    Rigidbody rb;

    float distGround;

    public int maxEnergy;
    public int energy;
    public float timeLastEnergyChange;
    public int rollCost;
    private float timeLastRoll;
    public float rollLen;
    public float rollDellay;

    //Control camera horizontale

    public float horizontaleSpeed;
    private float yaw;

    //Stats

    public int maxLife;
    public int life;

    //LifeBar

    public GameObject lifeBar;

    //Energy Bar

    public GameObject energyBar;
    //Combat

    public GameObject arrow;
    public float attackDelay;
    private float lastAttackTime;
    private bool invincible;

    //Animations

    private Animator animator;
    private GameObject gameOver;

    // Start is called before the first frame update
    void Start()
    {
        gameOver = GameObject.Find("GameOver");
        invincible = false;
        timeLastRoll = Time.time;
        moveSpeed = baseMoveSpeed;
        rb = GetComponent<Rigidbody>();
        distGround = GetComponent<Collider>().bounds.extents.y;
        isRunning = false;
        energy = maxEnergy;
        timeLastEnergyChange = Time.time;

        Cursor.lockState = CursorLockMode.Locked;
        yaw = 0f;

        lastAttackTime = Time.time;
        animator = gameObject.GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            isRunning = true;
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            isRunning = false;
        }


        if(isRunning && energy > 0)
        {
            moveSpeed = runningSpeed;
            RunningEnergyLoss();
        }
        else
        {
            moveSpeed = baseMoveSpeed;
            EnergyGain();
        }
        
        Movements();
        MovementsAnimations();

        LookAtMouse();

        if (Input.GetMouseButtonDown(1))
        {
            ShootArrow();
        }

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log(invincible);
        }

        UpdtateLifeBar();
        UpdtateEnergyBar();

        if(Input.GetKeyDown(KeyCode.Space) && IsGrounded())
        {   animator.SetTrigger("Jump");
            Invoke("Jump", 0.2f);
        }

        if(Input.GetKeyDown(KeyCode.LeftControl))
        {
            Roll();
        }

        if (Time.time - timeLastRoll < rollLen)
        {
            invincible = true;
        }
        else
        {
            invincible = false;
        }

    }

    void RunningEnergyLoss()
    {
        if(Time.time - timeLastEnergyChange > 0.1)
        {
            energy -= 1;
            timeLastEnergyChange = Time.time;
        }
    }

    void Roll()
    {
        if (energy >= rollCost && Time.time - timeLastRoll > rollDellay)
        {
            animator.SetTrigger("Roll");
            timeLastRoll = Time.time;
            gameObject.GetComponent<Rigidbody>().velocity = transform.forward.normalized * 11;
            energy -= rollCost;
        }
    }

    void EnergyGain()
    {
        if(Time.time - timeLastEnergyChange > 0.2 && !invincible)
        {
            energy += 1;
            if(energy > maxEnergy)
            {
                energy = maxEnergy;
            }
            timeLastEnergyChange = Time.time;
        }
    }

    void Movements()
    {
        if (Input.GetKey(KeyCode.Z))
        {
            transform.position += transform.forward * moveSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.position -= transform.forward  * moveSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.Q))
        {
            transform.position -= transform.right  * moveSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.position += transform.right  * moveSpeed * Time.deltaTime;
        }

    }

    void MovementsAnimations()
    {
        if (Input.GetKey(KeyCode.Z))
        {
            animator.SetBool("IsWalking", true);
        }
        if (Input.GetKeyUp(KeyCode.Z))
        {
            animator.SetBool("IsWalking", false);
        }

        if (Input.GetKey(KeyCode.LeftShift) && energy > 1)
        {
            animator.SetBool("IsRunning", true);
        }
        if (Input.GetKeyUp(KeyCode.LeftShift) || energy < 2)
        {
            animator.SetBool("IsRunning", false);
        }

        if (Input.GetKey(KeyCode.S))
        {
            animator.SetBool("IsWalking", true);
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            animator.SetBool("IsWalking", false);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            animator.SetBool("IsWalking", true);
        }
        if (Input.GetKeyUp(KeyCode.Q))
        {
            animator.SetBool("IsWalking", false);
        }

        if (Input.GetKey(KeyCode.D))
        {
            animator.SetBool("IsWalking", true);
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            animator.SetBool("IsWalking", false);
        }

    }

    bool IsGrounded()
    {
        return Physics.Raycast(transform.position + new Vector3(0, distGround, 0), Vector3.down, distGround + 0.1f);
    }

    void Jump()
    {

        gameObject.GetComponent<Rigidbody>().velocity = Vector3.up * jumpSpeed * 1;
    }
    void LookAtMouse()
    {
        yaw += horizontaleSpeed * Input.GetAxis("Mouse X");

        transform.eulerAngles = new Vector3(transform.eulerAngles.x, yaw, 0.0f);
    }

    void changeLife(int amount)
    {
        life += amount;
        if (life > maxLife)
        {
            life = maxLife;
        }
        if (life <= 0)
        {
            Death();
        }
    }

    public void takeDamages(int damages)
    {
        if(!invincible)
        {
            changeLife(-damages);
        }
    }

    public void heal(int amount)
    {
        changeLife(amount);
    }

    void UpdtateLifeBar()
    {
        float lifeRatio = (float) life/ (float) maxLife;
        lifeBar.GetComponent<UIHealthBar>().SetFillStatus(lifeRatio);
    }
    
    void UpdtateEnergyBar()
    {
        float energyRatio = (float) energy/ (float) maxEnergy;
        energyBar.GetComponent<UIHealthBar>().SetFillStatus(energyRatio);
    }

    void ShootArrow()
    {
        if (Time.time - lastAttackTime > attackDelay && !invincible)
        {
            animator.SetTrigger("Shoot");
            Instantiate(arrow, transform.position + transform.forward *2f, transform.rotation);
            lastAttackTime = Time.time;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent<MeshCollider>(out var floor))
        {
            landed = true;
            //animator.ResetTrigger("Jump");
        }
    }


    void Death()
    {
        gameOver.GetComponent<TMPro.TextMeshProUGUI>().enabled = true;
        animator.SetTrigger("Death");
    }
    
}
