using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject playerM;
    private float pitch;
    public float verticaleSpeed;
    public float pitchDelta;
    

    private bool walled;

    // Start is called before the first frame update
    void Start()
    {
        pitch = 0;
        walled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(walled)
        {
            transform.position -= new Vector3(0, 4, 0);
            walled = false;
        }

        pitch -= verticaleSpeed * Input.GetAxis("Mouse Y");
        if(pitch > pitchDelta)
        {
            pitch = pitchDelta;
        }
        if(pitch < -pitchDelta)
        {
            pitch = -pitchDelta;
        }
        transform.eulerAngles = new Vector3(pitch, transform.eulerAngles.y, transform.eulerAngles.z);

        checkWalls();
    }

    void checkWalls()
    {

        Vector3 traj = playerM.transform.position - transform.position;
        float dist = Mathf.Sqrt(traj.sqrMagnitude);
        if (Physics.Raycast(transform.position, traj, 5))
        {
            transform.eulerAngles += new Vector3(45, 0, 0);
            transform.position += new Vector3(0, 4, 0);
            walled = true;
        }
    }

}
