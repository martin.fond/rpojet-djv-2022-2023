using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject enemy;
    public GameObject enemies;
    // Start is called before the first frame update
    void Start()
    {
        enemies = GameObject.Find("Enemies");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SpawnEnemy()
    {
        var newEnemy = Instantiate(enemy, transform.position - transform.forward, transform.rotation);
        newEnemy.transform.SetParent(enemies.transform);
        enemies.GetComponent<Enemies>().enemiesNumber += 1;
    }
}
