using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Enemies : MonoBehaviour
{
    public int enemiesNumber;
    public int killCount;
    public int waveNumber;
    private GameObject[] spawners;
    private GameObject waveNumberDisplay;
    private GameObject killCountDisplay;
    private GameObject enemiesNumberDisplay;

    // Start is called before the first frame update
    void Start()
    {
        waveNumber = 1;
        enemiesNumber = 2;
        spawners = GameObject.FindGameObjectsWithTag("Spawner");
        killCount = 0;
        waveNumberDisplay = GameObject.Find("WaveNumber");
        killCountDisplay = GameObject.Find("KillCount");
        enemiesNumberDisplay = GameObject.Find("EnemiesCount");
    }

    // Update is called once per frame
    void Update()
    {

        if(enemiesNumber == 0)
        { 
            NewWave();
        }

        killCountDisplay.GetComponent<TMPro.TextMeshProUGUI>().text = "Kill Count : " + killCount.ToString();
        waveNumberDisplay.GetComponent<TMPro.TextMeshProUGUI>().text = "Wave : " + waveNumber.ToString();
        enemiesNumberDisplay.GetComponent<TMPro.TextMeshProUGUI>().text = "Enemies count : " + enemiesNumber.ToString();
    }

    public void DecreaseEnemiesNumber()
    {
        enemiesNumber -= 1;
    }

    private void SpawnEnemies()
    {
        foreach(GameObject spawner in spawners )
        {
            spawner.GetComponent<Spawner>().SpawnEnemy();
        }
    }

    private void NewWave()
    {
        for (int i = 0; i < waveNumber; i++)
        {
            Invoke("SpawnEnemies", i);
        }
        waveNumber += 1;
    }
}
