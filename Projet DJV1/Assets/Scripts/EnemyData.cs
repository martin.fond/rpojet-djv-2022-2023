using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Enemy", menuName = "Game/Enemy")]

public class EnemyData : ScriptableObject
{
    public int maxLife;
    public int moveSpeed;
    public int attackDamages;
    public float attackDelay;
}