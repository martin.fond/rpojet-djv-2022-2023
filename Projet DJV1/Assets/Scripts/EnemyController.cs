using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public GameObject lifeBar;
    public EnemyData enemyData;
    private Animator animator;
    public int maxLife;
    public int life;

    public GameObject healingPotion;

    public GameObject enemies;

    public bool isDead;

    // Start is called before the first frame update
    void Start()
    {
        maxLife = enemyData.maxLife;
        life = maxLife;
        lifeBar = transform.GetChild(1).gameObject;
        animator = gameObject.GetComponentInChildren<Animator>();
        isDead = false;
        enemies = GameObject.Find("Enemies");

    }

    // Update is called once per frame
    void Update()
    {
        UpdtateLifeBar();
    }

    public void TakeDamages(int damage)
    {
        life -= damage;
        if (life <= 0)
        {
            Death();
        }
    }

    void NewDestroy()
    {
        Destroy(gameObject);
    }

    void Death()
    {
        if (!isDead)
        {
        Destroy(gameObject.GetComponent<Rigidbody>());
        enemies.GetComponent<Enemies>().DecreaseEnemiesNumber();
        enemies.GetComponent<Enemies>().killCount += 1;
        Invoke("lootOnDeath", 2f);
        Invoke("NewDestroy", 2f);
        animator.SetTrigger("Death");
        isDead = true;
        }
 
    }

    void UpdtateLifeBar()
    {
        if (life == maxLife)
        {
            lifeBar.SetActive(false);
        }
        else
        {
            lifeBar.SetActive(true);
            float lifeRatio = (float) life/ (float) maxLife;
            lifeBar.GetComponent<UIHealthBar>().SetFillStatus(lifeRatio);
        }

    }

    void lootOnDeath()
    {
        int lootRes = Random.Range(0, 6);
        if (lootRes == 0)
        {
            Instantiate(healingPotion, new Vector3(transform.position.x, 0, transform.position.z), transform.rotation);
        }
    }

}
