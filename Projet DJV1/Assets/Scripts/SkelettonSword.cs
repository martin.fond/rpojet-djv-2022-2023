using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkelettonSword : MonoBehaviour
{
    public int attackDamages;
    public float attackDelay;
    public float lastAttackTime;
    private bool playerInRange;
    public int moveSpeed;

    public EnemyData enemyData;
    public GameObject player;
    private Animator animator;
    private new Rigidbody rigidbody;




    // Start is called before the first frame update
    void Start()
    {
        rigidbody = gameObject.GetComponent<Rigidbody>();
        attackDamages = enemyData.attackDamages;
        attackDelay = enemyData.attackDelay;
        lastAttackTime = Time.time;
        animator = gameObject.GetComponentInChildren<Animator>();
        playerInRange = false;
        moveSpeed = enemyData.moveSpeed;
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameObject.GetComponent<EnemyController>().isDead)
        {
            move();
        }
    }

    IEnumerator Attack(PlayerController playerC)
    {
        for (int i = 0; i < 2; i++)
        {
            if(i == 1)
            {
                playerC.takeDamages(attackDamages);
            }
            yield return new WaitForSeconds(0.3f);
        }

    }

    void AttackPlayer(PlayerController playerC)
    {
        if (Time.time - lastAttackTime > attackDelay)
        {
        animator.SetTrigger("Attack");
        StartCoroutine(Attack(playerC));
        lastAttackTime = Time.time;
        }
    }

    private void OnTriggerStay(Collider other) 
    {
        if(other.gameObject.TryGetComponent<PlayerController>(out var playerC))
        {
            playerInRange = true;
            AttackPlayer(playerC);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.TryGetComponent<PlayerController>(out var player))
        {
            playerInRange = false;
        }        
    }


    void move()
    {
        if (!playerInRange)
        {
            Vector3 direction = player.transform.position - transform.position;
            transform.position += new Vector3(direction.normalized.x, 0, direction.normalized.z) * Time.deltaTime * moveSpeed;
            transform.rotation = Quaternion.LookRotation(direction);
            animator.SetBool("IsWalking", true);
        }
        else
        {
            animator.SetBool("IsWalking", false);    
        }
    }

}
